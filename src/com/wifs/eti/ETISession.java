package com.wifs.eti;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

import java.io.DataInputStream;
import java.net.Socket;
import org.apache.log4j.Logger;
import java.util.Arrays;


public class ETISession {

    private static Logger logger = Logger.getLogger(ETISession.class);

    private Socket socket = null;
    private BufferedOutputStream bufOS = null;
    private BufferedInputStream bufIS = null;
    boolean fConnect = false;
    ETIRequest etiRequest = new ETIRequest();
    ETIResponse etiResponse = new ETIResponse();

    private String host = null;
    private int port = 0;

    public boolean createConnection() {
        try {

            host = etiResponse.getHost();
            port = etiResponse.getPort();

            logger.debug("Creating ETI connection: " + host + "," + port);

            socket = new Socket(host, port);

            bufOS = new BufferedOutputStream(socket.getOutputStream());
            bufIS = new BufferedInputStream(socket.getInputStream());

            System.out.println(socket.isConnected());

            fConnect = true;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return fConnect;
    }

    public void sendMessage(byte[] bytes) {
        try {
    //        System.out.println("Message to be Sent to ETI Server");
    //        System.out.println("--------------------------------" + this);
            bufOS.write(bytes);
            bufOS.flush();
            System.out.println("---------------------------------------");
            System.out.println("Message Sent to ETI Server");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }


    public byte[] receiveMessage() {

        byte[] received = null;

        try {
            //System.out.println("Message from ETI awaited: BufIS");
            //System.out.println("--------------------------------" + etiResponse.getHost() + "," + etiResponse.getPort());

               // TimeUnit.SECONDS.sleep(10);

                //System.out.println("BufIS is: " + bufIS);

                if (bufIS != null) {

                    DataInputStream dis = new DataInputStream(bufIS);

                        if (dis != null) {
                            int length = dis.available();

                            if (length > 0) {
                                System.out.println("Input Stream from ETI length: " + length);
                                System.out.println("---------------------------------------");
                            }

                            if (length > 0) {
                                received = new byte[length];
                                dis.readFully(received);
                            }
                        }

                        //TimeUnit.SECONDS.sleep(1000);

                        if (received != null) {
                            System.out.println(Arrays.toString(received));
                            etiResponse.processResponse(received);
                            //received = null;
                        }
                    }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return received;
    }

    public ETIRequest getEtiRequest(){
        return etiRequest;
    }

    public ETIResponse getEtiResponse(){
        return etiResponse;
    }

}