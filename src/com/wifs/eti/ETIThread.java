package com.wifs.eti;

public class ETIThread extends Thread {

    private ETISession etiSession = null;

    public ETIThread(ETISession etiSession) {
        this.etiSession = etiSession;
    }

    @Override
    public void run() {
        try {
            while (etiSession != null) {
                this.etiSession.receiveMessage();
                Thread.sleep(100);
            }
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
