package com.wifs.eti;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;

import static com.wifs.eti.ETIUtil.*;

public class ETIResponse {

    public int getByteArrForUserLogonResponse(byte[] etiUserLogonResponse) {

        byte[] returnMsg = new byte[48];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //request header
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[4];
        byte[] array6 = new byte[4];

        //message body
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[1];
        byte[] array9 = new byte[1];
        byte[] array10 = new byte[6];

        if (etiUserLogonResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiUserLogonResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiUserLogonResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiUserLogonResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array3 = Arrays.copyOfRange(etiUserLogonResponse, 8, 16);
            String rTime = fromByteArraytoTimeStamp(array3);

            array4 = Arrays.copyOfRange(etiUserLogonResponse, 16, 24);
            String sTime = fromByteArraytoTimeStamp(array4);

            array5 = Arrays.copyOfRange(etiUserLogonResponse, 24, 28);
            int MsgSeqNum = fromByteArraytoInt(array5);

            switch (TemplateID) {

                case 10019:
                    System.out.println("User Logon Response received");
                    System.out.println("User Logon Response message no is: " + MsgSeqNum);
                    System.out.println("User Logon Response CG Request time is: " + rTime);
                    System.out.println("User Logon Response CG Send time is: " + sTime);
                    break;

                default:
                    System.out.println("TemplateID:" + TemplateID);
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiUserLogonResponse.length - BodyLen);

    }



    public int getByteArrForUserLogoutResponse(byte[] etiUserLogoutResponse) {

        byte[] returnMsg = new byte[32];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //request header
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[4];
        byte[] array6 = new byte[4];

        //message body
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[1];
        byte[] array9 = new byte[1];
        byte[] array10 = new byte[6];

        if (etiUserLogoutResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiUserLogoutResponse.length);


        try {

            array0 = Arrays.copyOfRange(etiUserLogoutResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiUserLogoutResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array3 = Arrays.copyOfRange(etiUserLogoutResponse, 8, 16);
            String rTime = fromByteArraytoTimeStamp(array3);

            array4 = Arrays.copyOfRange(etiUserLogoutResponse, 16, 24);
            String sTime = fromByteArraytoTimeStamp(array4);

            array5 = Arrays.copyOfRange(etiUserLogoutResponse, 24, 28);
            int MsgSeqNum = fromByteArraytoInt(array5);

            switch (TemplateID) {

                case 10024:
                    System.out.println("User Logout Response received");
                    System.out.println("User Logout Response message no is: " + MsgSeqNum);
                    System.out.println("User Logout Response CG Request time is: " + rTime);
                    System.out.println("User Logout Response CG Send time is: " + sTime);
                    break;

                default:
                    System.out.println("TemplateID:" + TemplateID);
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiUserLogoutResponse.length - BodyLen);

    }



    //Simulation

    private String Host = "10.255.254.6";
    private int Port = 12408;


    //Live Server
    /*
    private String Host = "10.255.255.6";
    private int Port = 12908;
    */

    public String getHost(){
        return Host;
    }
    public int getPort(){
        return Port;
    }



    public int getByteArrForConnectionGatewayResponse(byte[] etiConnectionGatewayResponse) {

        byte[] returnMsg = new byte[56];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //request header
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[4];
        byte[] array6 = new byte[4];

        //message body
        byte[] array7 = new byte[4];
        byte[] array8 = new byte[4];
        byte[] array9 = new byte[4];
        byte[] array10 = new byte[4];
        byte[] array11 = new byte[1];
        byte[] array12 = new byte[1];
        byte[] array13 = new byte[6];

        if (etiConnectionGatewayResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiConnectionGatewayResponse.length);


        try {

            array0 = Arrays.copyOfRange(etiConnectionGatewayResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiConnectionGatewayResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array7 = Arrays.copyOfRange(etiConnectionGatewayResponse, 32, 36);
            Host = fromByteArraytoIPAddress(array7);

            array8 = Arrays.copyOfRange(etiConnectionGatewayResponse, 36, 40);
            Port = fromByteArraytoInt(array8);

            //TODO: Also get secondary server and port

            switch (TemplateID) {

                case 10021:
                    System.out.println("Exchange IP Address, Port is:" + Host + "," + Port);
                    System.out.println("Response TemplateID is: " + TemplateID);
                    break;

                default:
                    System.out.println("TemplateID:" + TemplateID);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiConnectionGatewayResponse.length - BodyLen);

    }



    public int getByteArrForSessionHeartbeatNotification (byte[] etiSessionHeatBeatNotification) {

        byte[] returnMsg = new byte[16];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //notification header
        byte[] array3 = new byte[8];

        if (etiSessionHeatBeatNotification == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiSessionHeatBeatNotification.length);


        try {

            array0 = Arrays.copyOfRange(etiSessionHeatBeatNotification, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiSessionHeatBeatNotification, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("IST"));

            array3 = Arrays.copyOfRange(etiSessionHeatBeatNotification, 8, 16);
            String sTimeStr = fromByteArraytoTimeStamp(array3);

            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10023:
                    System.out.println("Session Heartbeat Notification received");
                    System.out.println("Session Heartbeat Notification sending time: " + sTimeStr);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiSessionHeatBeatNotification.length - BodyLen);

    }



    public int getByteArrForSessionLogoutNotification(byte[] etiSessionLogoutNotification) {

        byte[] returnMsg = new byte[2024];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //notification header
        byte[] array3 = new byte[8];

        //message body
        byte[] array4 = new byte[2];
        byte[] array5 = new byte[6];
        byte[] array6 = new byte[2000];

        if (etiSessionLogoutNotification == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiSessionLogoutNotification.length);

        try {

            array0 = Arrays.copyOfRange(etiSessionLogoutNotification, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiSessionLogoutNotification, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("IST"));

            array3 = Arrays.copyOfRange(etiSessionLogoutNotification, 8, 16);
            long sTime = fromByteArraytoLong(array3);
            String sTimeStr = sdf.format(new Date(Long.valueOf(sTime/1000000)));

            array4 = Arrays.copyOfRange(etiSessionLogoutNotification, 16, 18);
            int VarTextLen = fromByteArraytoInt(array4);

            VarTextLen = 24 + VarTextLen;

            array6 = Arrays.copyOfRange(etiSessionLogoutNotification, 24, 2000);
            String errMessage=fromByteArrayToStringWithEncode(array6, "UTF-8");

            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10012:
                    System.out.println("Session Logout Notification sending time: " + sTimeStr);
                    System.out.println("Session Logout Notification errMessage Length: " + VarTextLen);
                    System.out.println("Session Logout Notification errMessage: " + errMessage);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiSessionLogoutNotification.length - BodyLen);

    }


    public int getByteArrForSessionLogoutResponse(byte[] etiSessionLogoutResponse) {

        byte[] returnMsg = new byte[32];
        int BodyLen = 0;
        int TemplateID = 0;
        
        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //request header
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[4];
        byte[] array6 = new byte[4];

        if (etiSessionLogoutResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiSessionLogoutResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiSessionLogoutResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiSessionLogoutResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("IST"));

            array3 = Arrays.copyOfRange(etiSessionLogoutResponse, 8, 16);
            long rTime = fromByteArraytoLong(array3);
            String rTimeStr = sdf.format(new Date(Long.valueOf(rTime/1000000)));

            array4 = Arrays.copyOfRange(etiSessionLogoutResponse, 16, 24);
            long sTime = fromByteArraytoLong(array4);
            String sTimeStr = sdf.format(new Date((Long.valueOf(sTime/1000000))));

            array5 = Arrays.copyOfRange(etiSessionLogoutResponse, 24, 28);
            int MsgSeqNum = fromByteArraytoInt(array5);

            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10003:
                    System.out.println("Session Logout request time is: " + rTimeStr);
                    System.out.println("Session Logout sending time is: " + sTimeStr);
                    System.out.println("Session Logout msg sequence no is: " + MsgSeqNum);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiSessionLogoutResponse.length - BodyLen);

    }


    public int getByteArrforSubscribeResponse(byte[] etiSubscribeResponse) {

        byte[] returnMsg = new byte[104];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //request header
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[4];
        byte[] array6 = new byte[4];

        //message body
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[4];
        byte[] array9 = new byte[4];
        byte[] array10 = new byte[4];
        byte[] array11 = new byte[4];
        byte[] array12 = new byte[4];
        byte[] array13 = new byte[4];
        byte[] array14 = new byte[1];
        byte[] array15 = new byte[1];
        byte[] array16 = new byte[1];
        byte[] array17 = new byte[1];
        byte[] array18 = new byte[30];
        byte[] array19 = new byte[2];


        if (etiSubscribeResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiSubscribeResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiSubscribeResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiSubscribeResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array7 = Arrays.copyOfRange(etiSubscribeResponse, 32, 36);
            int ApplSubID = fromByteArraytoInt(array7);


            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10005:
                    System.out.println("Subscribe Response: " + ApplSubID);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiSubscribeResponse.length - BodyLen);
    }



    public int getByteArrForSessionLogonResponse(byte[] etiSessionLogonResponse) {

        byte[] returnMsg = new byte[104];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //request header
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[4];
        byte[] array6 = new byte[4];

        //message body
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[4];
        byte[] array9 = new byte[4];
        byte[] array10 = new byte[4];
        byte[] array11 = new byte[4];
        byte[] array12 = new byte[4];
        byte[] array13 = new byte[4];
        byte[] array14 = new byte[1];
        byte[] array15 = new byte[1];
        byte[] array16 = new byte[1];
        byte[] array17 = new byte[1];
        byte[] array18 = new byte[30];
        byte[] array19 = new byte[2];

        if (etiSessionLogonResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiSessionLogonResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiSessionLogonResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiSessionLogonResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array12 = Arrays.copyOfRange(etiSessionLogonResponse, 60, 64);
            int HeartBtInt = fromByteArraytoInt(array12);

            array13 = Arrays.copyOfRange(etiSessionLogonResponse, 64, 68);
            int SessionInstanceID = fromByteArraytoInt(array13);

            array14 = Arrays.copyOfRange(etiSessionLogonResponse, 68, 69);
            int TradSesMode = fromByteArraytoInt(array14);

            array18 = Arrays.copyOfRange(etiSessionLogonResponse, 72, 102);
            String DefaultCstmAppVerID = fromByteArrayToStringWithEncode(array18, "UTF-8");


            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10001:
                    System.out.println("HearBeat Interval advised is: " + HeartBtInt);
                    System.out.println("UniqueID for session instance assigned is: " + SessionInstanceID);
                    System.out.println("Trading session mode is: " + TradSesMode);
                    System.out.println("Default Custom Application Version is :" + DefaultCstmAppVerID);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiSessionLogonResponse.length - BodyLen);

    }



    public int getByteArrForImmediateExecutionResponse(byte[] etiImmediateExecutionResponse) {

        byte[] returnMsg = new byte[1024];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //message response
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[8];
        byte[] array6 = new byte[8];
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[8];

        byte[] array9 = new byte[4];
        byte[] array10 = new byte[2];
        byte[] array11 = new byte[1];
        byte[] array12 = new byte[16];
        byte[] array13 = new byte[1];

        byte[] array14 = new byte[8];
        byte[] array15 = new byte[8];
        byte[] array16 = new byte[8];
        byte[] array17 = new byte[8];
        byte[] array18 = new byte[8];
        byte[] array19 = new byte[8];
        byte[] array20 = new byte[8];
        byte[] array21 = new byte[8];
        byte[] array22 = new byte[8];

        byte[] array23 = new byte[4];
        byte[] array24 = new byte[4];
        byte[] array25 = new byte[4];
        byte[] array26 = new byte[4];
        byte[] array27 = new byte[4];
        byte[] array28 = new byte[4];
        byte[] array29 = new byte[2];
        byte[] array30 = new byte[2];

        byte[] array31 = new byte[1];
        byte[] array32 = new byte[1];
        byte[] array33 = new byte[1];
        byte[] array34 = new byte[1];
        byte[] array35 = new byte[1];
        byte[] array36 = new byte[16];
        byte[] array37 = new byte[7];


        if (etiImmediateExecutionResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiImmediateExecutionResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiImmediateExecutionResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiImmediateExecutionResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array12 = Arrays.copyOfRange(etiImmediateExecutionResponse, 60, 62);
            int PartitionID = fromByteArraytoInt(array12);

            array13 = Arrays.copyOfRange(etiImmediateExecutionResponse, 80, 88);
            OrderID = fromByteArraytoLong(array13);

            array14 = Arrays.copyOfRange(etiImmediateExecutionResponse, 88, 96);
            long ClOrdID = fromByteArraytoLong(array14);

            array18 = Arrays.copyOfRange(etiImmediateExecutionResponse, 104, 112);
            int InstrumentID = fromByteArraytoInt(array18);

            array20 = Arrays.copyOfRange(etiImmediateExecutionResponse, 136, 144);
            ActivityTime = fromByteArraytoLong(array20);

            array24 = Arrays.copyOfRange(etiImmediateExecutionResponse, 164,168);
            int LeavesQty = fromByteArraytoInt(array24);

            array25 = Arrays.copyOfRange(etiImmediateExecutionResponse, 168,172);
            int CumQty = fromByteArraytoInt(array25);

            array26 = Arrays.copyOfRange(etiImmediateExecutionResponse, 172,176);
            int CxlQty = fromByteArraytoInt(array26);

            array29 = Arrays.copyOfRange(etiImmediateExecutionResponse, 178,180);
            int ExecRestatementReason = fromByteArraytoInt(array29);

            array30 = Arrays.copyOfRange(etiImmediateExecutionResponse, 181,182);
            int OrdStatus = fromByteArraytoInt(array30);

            array31 = Arrays.copyOfRange(etiImmediateExecutionResponse, 184,185);
            int NoFills = fromByteArraytoInt(array31);

            array34 = Arrays.copyOfRange(etiImmediateExecutionResponse, 208,216);
            long Price = fromByteArraytoLong(array34);

            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10103:
                    System.out.println("Partition ID is: " + PartitionID);
                    System.out.println("OrderID is: " + OrderID);
                    System.out.println("ClOrdID is: " + ClOrdID);
                    System.out.println("InstrumentID is :" + InstrumentID);
                    System.out.println("Filled Qty: " + CumQty);
                    System.out.println("Pending Qty is :" + LeavesQty);
                    System.out.println("Cancelled Qty is :" + CxlQty);
                    System.out.println("Execution Status is: " + ExecRestatementReason);
                    System.out.println("Order Status is: " + OrdStatus);
                    System.out.println("No of fills is: " + NoFills);
                    System.out.println("Price is:" + Price);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiImmediateExecutionResponse.length - BodyLen);

    }



    public void processResponse (byte[] response) {


        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];

        if (response == null)
            return;

        try {

            System.out.println("---------------------------------------");
            System.out.println("RESPONSE MESSAGE RECEIVED IS: " + response.length);

            int recur = 0;
            byte[] innerResponse = new byte[response.length];

            innerResponse = Arrays.copyOfRange(response, recur, response.length);

                    do {

                       // System.out.println("Response RECUR is: " + recur);
                       // System.out.println("Response LENGTH BEFORE is: " + innerResponse.length);

                        array0 = Arrays.copyOfRange(innerResponse, 0, 4);
                        BodyLen = fromByteArraytoInt(array0);

                        array1 = Arrays.copyOfRange(innerResponse, 4, 6);
                        TemplateID = fromByteArraytoInt(array1);

                        System.out.println("---------------------------------------");
                        System.out.println("Response TemplateID is: " + TemplateID);

                        switch (TemplateID) {
                            case 10010:
                                System.out.println("Reject Message received");
                                recur = getRejectResponse(innerResponse);
                                break;

                            case 10990:
                                System.out.println("Order Confirmation received");
                                recur = getByteArrForOrderConfirmation(innerResponse);
                                break;

                            case 10003:
                                System.out.println("Session Logout Message received");
                                recur = getByteArrForSessionLogoutResponse(innerResponse);
                                break;

                            case 10021:
                                System.out.println("Connection Gateway Message received");
                                recur = getByteArrForConnectionGatewayResponse(innerResponse);
                                break;

                            case 10019:
                                System.out.println("User Logon Response received");
                                recur = getByteArrForUserLogonResponse(innerResponse);
                                break;

                            case 10001:
                                System.out.println("Session Logon Message received");
                                recur = getByteArrForSessionLogonResponse(innerResponse);
                                break;

                            case 10024:
                                System.out.println("User Logout Response received");
                                recur = getByteArrForUserLogoutResponse(innerResponse);
                                break;

                            case 10023:
                                System.out.println("Heartbeat Notification");
                                recur = getByteArrForSessionHeartbeatNotification(innerResponse);
                                break;

                            case 10102:
                                System.out.println("New Order Response Lean");
                                recur = getByteArrForNewOrderLeanResponse(innerResponse);
                                break;

                            case 10012:
                                System.out.println("Session Logout Notification");
                                recur = getByteArrForSessionLogoutNotification(innerResponse);
                                break;

                            case 10005:
                                System.out.println("Subscribe Broadcast Response");
                                recur = getByteArrforSubscribeResponse(innerResponse);
                                break;

                            case 10103:
                                System.out.println("Immediate Execution Response");
                                recur = getByteArrForImmediateExecutionResponse(innerResponse);
                                break;

                            case 10111:
                                System.out.println("Cancel Order Lean Response");
                                recur = getByteArrForCancelOrderLeanResponse(innerResponse);
                                break;

                            case 10108:
                                System.out.println("Replace Order Lean Response");
                                recur = getByteArrForReplaceOrderLeanResponse(innerResponse);
                                break;

                            case 10500:
                                System.out.println("Trade Notification");
                                recur = getByteArrForTradeNotification(innerResponse);
                                break;

                            case 10101:
                                System.out.println("New Order Standard Response");
                                recur = getByteArrForNewOrderStandardResponse(innerResponse);
                                break;

                            case 10107:
                                System.out.println("Replace Order Standard Response");
                                recur = getByteArrForReplaceOrderStandardResponse(innerResponse);
                                break;

                            case 10110:
                                System.out.println("Cancel Order Standard Response");
                                recur = getByteArrForCancelOrderStandardResponse(innerResponse);
                                break;

                            case 10104:
                                System.out.println("Book Order Execution");
                                recur = getByteArrForBookOrderExecutionResponse(innerResponse);
                                break;

                            default:
                                System.out.println("MISCELLANEOUS Response NOT received");
                                System.out.println("TemplateID is: " + TemplateID);
                                recur = 0;
                                break;
                        }

                        if (recur > 0) {
                            innerResponse = Arrays.copyOfRange(innerResponse, innerResponse.length - recur, innerResponse.length);
                           // System.out.println("Response LENGTH AFTER is: " + innerResponse.length);
                        }

                    } while (recur > 0);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return;

    }


    long PrimaryOrderID = 0L;

    public long getPrimaryOrderID() {
        return PrimaryOrderID;
    }


    long OrderID = 0L;

    public long getOrderID() {
        return OrderID;
    }

    long ActivityTime = 0L;

    public long getActivityTime() {
        return ActivityTime;
    }


    public int getByteArrForOrderConfirmation(byte[] etiOrderConfirmationResponse) {

        byte[] returnMsg = new byte[56];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //request header
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[4];
        byte[] array6 = new byte[4];

        //message body
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[8];
        byte[] array9 = new byte[4];
        byte[] array10 = new byte[4];

        if (etiOrderConfirmationResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiOrderConfirmationResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiOrderConfirmationResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiOrderConfirmationResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array5 = Arrays.copyOfRange(etiOrderConfirmationResponse, 24, 28);
            int MsgSeqNum = fromByteArraytoInt(array5);

            array7 = Arrays.copyOfRange(etiOrderConfirmationResponse, 32, 40);
            PrimaryOrderID = fromByteArraytoLong(array7);

            array8 = Arrays.copyOfRange(etiOrderConfirmationResponse, 40, 48);
            long ClOrdID = fromByteArraytoLong(array8);

            switch (TemplateID) {

                case 10990:
                    System.out.println("TemplateID:" + TemplateID);
                    System.out.println("Order Confirmation Message No is: " + MsgSeqNum);
                    System.out.println("Order Confirmation Primary OrderID is: " + PrimaryOrderID);
                    System.out.println("Client Order ID is: " + ClOrdID);
                    break;


                default:
                    System.out.println("TemplateID:" + TemplateID);
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiOrderConfirmationResponse.length - BodyLen);

    }

    public int getByteArrForNewOrderLeanResponse(byte[] etiNewOrderSingleLeanResponse) {

        byte[] returnMsg = new byte[152];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //message response
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[8];
        byte[] array6 = new byte[8];
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[8];

        byte[] array9 = new byte[4];
        byte[] array10 = new byte[1];
        byte[] array11 = new byte[3];
        byte[] array12 = new byte[8];
        byte[] array13 = new byte[8];
        byte[] array14 = new byte[8];
        byte[] array15 = new byte[8];

        byte[] array16 = new byte[8];
        byte[] array17 = new byte[8];
        byte[] array18 = new byte[8];
        byte[] array19 = new byte[8];
        byte[] array20 = new byte[8];

        byte[] array21 = new byte[8];
        byte[] array22 = new byte[4];
        byte[] array23 = new byte[4];
        byte[] array24 = new byte[1];
        byte[] array25 = new byte[1];
        byte[] array26 = new byte[2];

        byte[] array27 = new byte[1];
        byte[] array28 = new byte[3];

        if (etiNewOrderSingleLeanResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiNewOrderSingleLeanResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiNewOrderSingleLeanResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiNewOrderSingleLeanResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array12 = Arrays.copyOfRange(etiNewOrderSingleLeanResponse, 64, 72);
            OrderID = fromByteArraytoLong(array12);

            array13 = Arrays.copyOfRange(etiNewOrderSingleLeanResponse, 72, 80);
            long ClOrdID = fromByteArraytoLong(array13);

            array14 = Arrays.copyOfRange(etiNewOrderSingleLeanResponse, 80, 88);
            int SecurityID = fromByteArraytoInt(array14);

            array23 = Arrays.copyOfRange(etiNewOrderSingleLeanResponse, 144, 145);
            String OrdStatus = fromByteArrayToStringWithEncode(array23, "US-ASCII");

            array24 = Arrays.copyOfRange(etiNewOrderSingleLeanResponse, 145, 146);
            String ExecType = fromByteArrayToStringWithEncode(array24,"US-ASCII");

            array25 = Arrays.copyOfRange(etiNewOrderSingleLeanResponse, 146, 148);
            int ExecRestatementReason = fromByteArraytoInt(array25);

            array19 = Arrays.copyOfRange(etiNewOrderSingleLeanResponse,120, 128);
            ActivityTime= fromByteArraytoLong(array19);

            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10102:
                    System.out.println("OrderID is: " + OrderID);
                    System.out.println("ClOrdID is: " + ClOrdID);
                    System.out.println("InstrumentID is: " + SecurityID);
                    System.out.println("Execution Type is: " + ExecType);
                    System.out.println("Execution Status is: " + ExecRestatementReason);
                    System.out.println("Order Status is: " + OrdStatus);
                    System.out.println("Activity Time is: " + ActivityTime);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiNewOrderSingleLeanResponse.length - BodyLen);

    }


    public int getByteArrForNewOrderStandardResponse(byte[] etiNewOrderSingleStandardResponse) {

        byte[] returnMsg = new byte[184];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //message response
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[8];
        byte[] array6 = new byte[8];
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[8];

        byte[] array9 = new byte[4];
        byte[] array10 = new byte[2];
        byte[] array11 = new byte[1];
        byte[] array12 = new byte[16];
        byte[] array13 = new byte[1];
        byte[] array14 = new byte[8];

        byte[] array15 = new byte[8];
        byte[] array16 = new byte[8];
        byte[] array17 = new byte[8];
        byte[] array18 = new byte[8];
        byte[] array19 = new byte[8];
        byte[] array20 = new byte[8];
        byte[] array21 = new byte[8];
        byte[] array22 = new byte[8];
        byte[] array23 = new byte[8];
        byte[] array24 = new byte[8];

        byte[] array25 = new byte[4];
        byte[] array26 = new byte[4];
        byte[] array27 = new byte[1];
        byte[] array28 = new byte[1];

        byte[] array29 = new byte[2];
        byte[] array30 = new byte[1];
        byte[] array31 = new byte[3];


        if (etiNewOrderSingleStandardResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiNewOrderSingleStandardResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiNewOrderSingleStandardResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiNewOrderSingleStandardResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array14 = Arrays.copyOfRange(etiNewOrderSingleStandardResponse, 80, 88);
            OrderID = fromByteArraytoLong(array14);

            array15 = Arrays.copyOfRange(etiNewOrderSingleStandardResponse, 88, 96);
            long ClOrdID = fromByteArraytoLong(array15);

            array16 = Arrays.copyOfRange(etiNewOrderSingleStandardResponse, 96, 104);
            int SecurityID = fromByteArraytoInt(array16);

            array26 = Arrays.copyOfRange(etiNewOrderSingleStandardResponse, 176, 177);
            String OrdStatus = fromByteArrayToStringWithEncode(array26, "US-ASCII");

            array27 = Arrays.copyOfRange(etiNewOrderSingleStandardResponse, 177, 178);
            String ExecType = fromByteArrayToStringWithEncode(array27,"US-ASCII");

            array28 = Arrays.copyOfRange(etiNewOrderSingleStandardResponse, 178, 180);
            int ExecRestatementReason = fromByteArraytoInt(array28);

            array22 = Arrays.copyOfRange(etiNewOrderSingleStandardResponse,152, 160);
            ActivityTime= fromByteArraytoLong(array22);


            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10101:
                    System.out.println("OrderID is: " + OrderID);
                    System.out.println("ClOrdID is: " + ClOrdID);
                    System.out.println("InstrumentID is: " + SecurityID);
                    System.out.println("Execution Type is: " + ExecType);
                    System.out.println("Execution Status is: " + ExecRestatementReason);
                    System.out.println("Order Status is: " + OrdStatus);
                    System.out.println("Activity Time is: " + ActivityTime);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiNewOrderSingleStandardResponse.length - BodyLen);

    }



    public int getByteArrForReplaceOrderLeanResponse(byte[] etiReplaceOrderSingleLeanResponse) {

        byte[] returnMsg = new byte[176];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //message response
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[8];
        byte[] array6 = new byte[8];
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[8];

        byte[] array9 = new byte[4];
        byte[] array10 = new byte[1];
        byte[] array11 = new byte[3];

        byte[] array12 = new byte[8];

        byte[] array13 = new byte[8];
        byte[] array14 = new byte[8];
        byte[] array15 = new byte[8];
        byte[] array16 = new byte[8];

        byte[] array17 = new byte[8];
        byte[] array18 = new byte[8];
        byte[] array19 = new byte[8];
        byte[] array20 = new byte[8];
        byte[] array21 = new byte[8];

        byte[] array22 = new byte[4];
        byte[] array23 = new byte[4];
        byte[] array24 = new byte[1];
        byte[] array25 = new byte[1];
        byte[] array26 = new byte[2];

        byte[] array27 = new byte[1];
        byte[] array28 = new byte[1];

        byte[] array29 = new byte[1];
        byte[] array30 = new byte[1];

        byte[] array31 = new byte[1];
        byte[] array32 = new byte[7];


        if (etiReplaceOrderSingleLeanResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiReplaceOrderSingleLeanResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiReplaceOrderSingleLeanResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiReplaceOrderSingleLeanResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array12 = Arrays.copyOfRange(etiReplaceOrderSingleLeanResponse, 64, 72);
            OrderID = fromByteArraytoLong(array12);

            array13 = Arrays.copyOfRange(etiReplaceOrderSingleLeanResponse, 72, 80);
            long ClOrdID = fromByteArraytoLong(array13);

            array14 = Arrays.copyOfRange(etiReplaceOrderSingleLeanResponse, 88, 96);
            int SecurityID = fromByteArraytoInt(array14);

            array19 = Arrays.copyOfRange(etiReplaceOrderSingleLeanResponse, 128, 136);
            ActivityTime = fromByteArraytoLong(array19);

            array27 = Arrays.copyOfRange(etiReplaceOrderSingleLeanResponse, 164, 165);
            String OrdStatus = fromByteArrayToStringWithEncode(array27, "US-ASCII");

            array28 = Arrays.copyOfRange(etiReplaceOrderSingleLeanResponse, 165, 166);
            String ExecType = fromByteArrayToStringWithEncode(array28,"US-ASCII");

            array29 = Arrays.copyOfRange(etiReplaceOrderSingleLeanResponse, 166, 168);
            int ExecRestatementReason = fromByteArraytoInt(array29);

            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10108:
                    System.out.println("OrderID is: " + OrderID);
                    System.out.println("ClOrdID is: " + ClOrdID);
                    System.out.println("SecurityID is: " + SecurityID);
                    System.out.println("Execution Type is: " + ExecType);
                    System.out.println("Execution Status is: " + ExecRestatementReason);
                    System.out.println("Order Status is: " + OrdStatus);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiReplaceOrderSingleLeanResponse.length - BodyLen);

    }


    public int getByteArrForReplaceOrderStandardResponse(byte[] etiReplaceOrderSingleStandardResponse) {

        byte[] returnMsg = new byte[200];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //message response
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[8];
        byte[] array6 = new byte[8];
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[8];

        byte[] array9 = new byte[4];
        byte[] array10 = new byte[2];
        byte[] array11 = new byte[1];
        byte[] array12 = new byte[16];
        byte[] array13 = new byte[1];
        byte[] array14 = new byte[8];

        byte[] array15 = new byte[8];
        byte[] array16 = new byte[8];
        byte[] array17 = new byte[8];
        byte[] array18 = new byte[8];
        byte[] array19 = new byte[8];
        byte[] array20 = new byte[8];
        byte[] array21 = new byte[8];
        byte[] array22 = new byte[8];
        byte[] array23 = new byte[8];
        byte[] array24 = new byte[8];

        byte[] array25 = new byte[4];
        byte[] array26 = new byte[4];
        byte[] array27 = new byte[4];
        byte[] array28 = new byte[4];
        byte[] array29 = new byte[4];
        byte[] array30 = new byte[1];

        byte[] array31 = new byte[1];
        byte[] array32 = new byte[2];
        byte[] array33 = new byte[1];
        byte[] array34 = new byte[7];


        if (etiReplaceOrderSingleStandardResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiReplaceOrderSingleStandardResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiReplaceOrderSingleStandardResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiReplaceOrderSingleStandardResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array14 = Arrays.copyOfRange(etiReplaceOrderSingleStandardResponse, 80, 88);
            OrderID = fromByteArraytoLong(array14);

            array15 = Arrays.copyOfRange(etiReplaceOrderSingleStandardResponse, 88, 96);
            long ClOrdID = fromByteArraytoLong(array15);

            array16 = Arrays.copyOfRange(etiReplaceOrderSingleStandardResponse, 104, 112);
            int SecurityID = fromByteArraytoInt(array16);

            array23 = Arrays.copyOfRange(etiReplaceOrderSingleStandardResponse, 152, 160);
            ActivityTime = fromByteArraytoLong(array23);

            array26 = Arrays.copyOfRange(etiReplaceOrderSingleStandardResponse, 188, 189);
            String OrdStatus = fromByteArrayToStringWithEncode(array26, "US-ASCII");

            array27 = Arrays.copyOfRange(etiReplaceOrderSingleStandardResponse, 189, 190);
            String ExecType = fromByteArrayToStringWithEncode(array27,"US-ASCII");

            array28 = Arrays.copyOfRange(etiReplaceOrderSingleStandardResponse, 190, 192);
            int ExecRestatementReason = fromByteArraytoInt(array28);

            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10107:
                    System.out.println("OrderID is: " + OrderID);
                    System.out.println("ClOrdID is: " + ClOrdID);
                    System.out.println("InstrumentID is: " + SecurityID);
                    System.out.println("Execution Type is: " + ExecType);
                    System.out.println("Execution Status is: " + ExecRestatementReason);
                    System.out.println("Order Status is: " + OrdStatus);
                    System.out.println("Activity Time is: " + ActivityTime);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiReplaceOrderSingleStandardResponse.length - BodyLen);

    }



    public int getByteArrForCancelOrderLeanResponse(byte[] etiCancelOrderSingleLeanResponse) {

        byte[] returnMsg = new byte[120];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //message response
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[8];
        byte[] array6 = new byte[8];
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[8];

        byte[] array9 = new byte[4];
        byte[] array10 = new byte[1];
        byte[] array11 = new byte[3];
        byte[] array12 = new byte[8];
        byte[] array13 = new byte[8];
        byte[] array14 = new byte[8];
        byte[] array15 = new byte[8];
        byte[] array16 = new byte[8];

        byte[] array17 = new byte[8];
        byte[] array18 = new byte[8];
        byte[] array19 = new byte[8];
        byte[] array20 = new byte[8];
        byte[] array21 = new byte[8];

        byte[] array22 = new byte[4];
        byte[] array23 = new byte[4];
        byte[] array24 = new byte[1];
        byte[] array25 = new byte[1];
        byte[] array26 = new byte[2];

        byte[] array27 = new byte[1];
        byte[] array28 = new byte[3];

        if (etiCancelOrderSingleLeanResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiCancelOrderSingleLeanResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiCancelOrderSingleLeanResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiCancelOrderSingleLeanResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array12 = Arrays.copyOfRange(etiCancelOrderSingleLeanResponse, 64, 72);
            OrderID = fromByteArraytoLong(array12);

            array13 = Arrays.copyOfRange(etiCancelOrderSingleLeanResponse, 72, 80);
            long ClOrdID = fromByteArraytoLong(array13);

            array14 = Arrays.copyOfRange(etiCancelOrderSingleLeanResponse, 80, 88);
            int InstrumentID = fromByteArraytoInt(array14);

            array24 = Arrays.copyOfRange(etiCancelOrderSingleLeanResponse, 112, 113);
            String OrdStatus = fromByteArrayToStringWithEncode(array24, "US-ASCII");

            array25 = Arrays.copyOfRange(etiCancelOrderSingleLeanResponse, 113, 114);
            String ExecType = fromByteArrayToStringWithEncode(array25,"US-ASCII");

            array26 = Arrays.copyOfRange(etiCancelOrderSingleLeanResponse, 114, 116);
            int ExecRestatementReason = fromByteArraytoInt(array26);

            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10111:
                    System.out.println("OrderID is: " + OrderID);
                    System.out.println("ClOrdID is: " + ClOrdID);
                    System.out.println("InstrumentID is: " + InstrumentID);
                    System.out.println("Execution Type is: " + ExecType);
                    System.out.println("Execution Status is: " + ExecRestatementReason);
                    System.out.println("Order Status is: " + OrdStatus);
                    System.out.println("Activity Time is: " + ActivityTime);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiCancelOrderSingleLeanResponse.length - BodyLen);

    }




    public int getByteArrForCancelOrderStandardResponse(byte[] etiCancelOrderSingleStandardResponse) {

        byte[] returnMsg = new byte[136];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //message response
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[8];
        byte[] array6 = new byte[8];
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[8];

        byte[] array9 = new byte[4];
        byte[] array10 = new byte[2];
        byte[] array11 = new byte[1];
        byte[] array12 = new byte[16];
        byte[] array13 = new byte[1];
        byte[] array14 = new byte[8];

        byte[] array15 = new byte[8];
        byte[] array16 = new byte[8];
        byte[] array17 = new byte[8];
        byte[] array18 = new byte[8];
        byte[] array19 = new byte[4];
        byte[] array20 = new byte[4];
        byte[] array21 = new byte[2];

        byte[] array22 = new byte[4];
        byte[] array23 = new byte[4];
        byte[] array24 = new byte[1];
        byte[] array25 = new byte[1];
        byte[] array26 = new byte[2];

        byte[] array27 = new byte[1];
        byte[] array28 = new byte[3];

        if (etiCancelOrderSingleStandardResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiCancelOrderSingleStandardResponse.length);

        try {

            array0 = Arrays.copyOfRange(etiCancelOrderSingleStandardResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiCancelOrderSingleStandardResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array14 = Arrays.copyOfRange(etiCancelOrderSingleStandardResponse, 80, 88);
            OrderID = fromByteArraytoLong(array14);

            array15 = Arrays.copyOfRange(etiCancelOrderSingleStandardResponse, 88, 96);
            long ClOrdID = fromByteArraytoLong(array15);

            array17 = Arrays.copyOfRange(etiCancelOrderSingleStandardResponse, 104, 108);
            int securityID = fromByteArraytoInt(array17);

            array21 = Arrays.copyOfRange(etiCancelOrderSingleStandardResponse, 128, 129);
            String OrdStatus = fromByteArrayToStringWithEncode(array21, "US-ASCII");

            array22 = Arrays.copyOfRange(etiCancelOrderSingleStandardResponse, 129, 130);
            String ExecType = fromByteArrayToStringWithEncode(array22,"US-ASCII");

            array23 = Arrays.copyOfRange(etiCancelOrderSingleStandardResponse, 130, 132);
            int ExecRestatementReason = fromByteArraytoInt(array23);

            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10110:
                    System.out.println("OrderID is: " + OrderID);
                    System.out.println("ClOrdID is: " + ClOrdID);
                    System.out.println("InstrumentID is: " + securityID);
                    System.out.println("Execution Type is: " + ExecType);
                    System.out.println("Execution Status is: " + ExecRestatementReason);
                    System.out.println("Order Status is: " + OrdStatus);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiCancelOrderSingleStandardResponse.length - BodyLen);

    }



    public int getRejectResponse(byte[] rejectResponse) {

        byte[] returnMsg = new byte[56];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //request header
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[8];
        byte[] array6 = new byte[8];
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[8];
        byte[] array9 = new byte[4];
        byte[] array10 = new byte[1];
        byte[] array11 = new byte[3];

        //message body
        byte[] array12 = new byte[4];
        byte[] array13 = new byte[2];
        byte[] array14 = new byte[1];
        byte[] array15 = new byte[1];
        byte[] array16 = new byte[2000];

        if (rejectResponse == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + rejectResponse.length);

        try {

            array0 = Arrays.copyOfRange(rejectResponse, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(rejectResponse, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array12 = Arrays.copyOfRange(rejectResponse, 64, 68);
            int RejectReason = fromByteArraytoInt(array12);

            array14 = Arrays.copyOfRange(rejectResponse, 70, 71);
            int SessionStatus = fromByteArraytoInt(array14);

            array13 = Arrays.copyOfRange(rejectResponse, 68, 70);
            int VarTextLen = fromByteArraytoInt(array13);

            VarTextLen = VarTextLen + 72;

            array16 = Arrays.copyOfRange(rejectResponse, 72, VarTextLen);
            String errMessage=fromByteArrayToStringWithEncode(array16, "UTF-8");

            System.out.println("BodyLen, TemplateID, RejectReason, VarTextLen, SessionStatus, VarText: " + BodyLen + "," + TemplateID + "," + RejectReason + "," + VarTextLen + "," + SessionStatus + "," + errMessage);

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (rejectResponse.length - BodyLen);
    }


    public int getByteArrForTradeNotification(byte[] etiTradeNotification) {

        byte[] returnMsg = new byte[352];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //message response
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[4];
        byte[] array6 = new byte[2];
        byte[] array7 = new byte[1];
        byte[] array8 = new byte[1];
        byte[] array9 = new byte[1];
        byte[] array10 = new byte[8];

        byte[] array11 = new byte[8];
        byte[] array12 = new byte[8];

        byte[] array13 = new byte[8];
        byte[] array14 = new byte[8];
        byte[] array15 = new byte[8];
        byte[] array16 = new byte[8];
        byte[] array17 = new byte[8];
        byte[] array18 = new byte[8];
        byte[] array19 = new byte[8];
        byte[] array20 = new byte[8];
        byte[] array21 = new byte[8];
        byte[] array22 = new byte[8];

        byte[] array23 = new byte[4];
        byte[] array24 = new byte[4];
        byte[] array25 = new byte[4];
        byte[] array26 = new byte[4];
        byte[] array27 = new byte[4];
        byte[] array28 = new byte[4];
        byte[] array29 = new byte[2];
        byte[] array30 = new byte[2];

        byte[] array31 = new byte[1];
        byte[] array32 = new byte[1];
        byte[] array33 = new byte[1];
        byte[] array34 = new byte[4];
        byte[] array35 = new byte[4];
        byte[] array36 = new byte[4];
        byte[] array37 = new byte[7];
        byte[] array38 = new byte[4];
        byte[] array39 = new byte[7];
        byte[] array40 = new byte[7];


        byte[] array41 = new byte[1];
        byte[] array42 = new byte[1];
        byte[] array43 = new byte[1];
        byte[] array44 = new byte[1];
        byte[] array45 = new byte[1];
        byte[] array46 = new byte[16];
        byte[] array47 = new byte[7];
        byte[] array48 = new byte[16];
        byte[] array49 = new byte[7];
        byte[] array50 = new byte[7];

        byte[] array51 = new byte[1];
        byte[] array52 = new byte[1];
        byte[] array53 = new byte[1];
        byte[] array54 = new byte[1];
        byte[] array55 = new byte[1];
        byte[] array56 = new byte[1];
        byte[] array57 = new byte[8];
        byte[] array58 = new byte[6];
        byte[] array59 = new byte[7];
        byte[] array60 = new byte[7];

        byte[] array61 = new byte[1];
        byte[] array62 = new byte[1];
        byte[] array63 = new byte[12];
        byte[] array64 = new byte[1];
        byte[] array65 = new byte[1];
        byte[] array66 = new byte[16];
        byte[] array67 = new byte[7];
        byte[] array68 = new byte[1];
        byte[] array69 = new byte[1];
        byte[] array70 = new byte[7];

        byte[] array71 = new byte[1];
        byte[] array72 = new byte[1];
        byte[] array73 = new byte[1];
        byte[] array74 = new byte[1];
        byte[] array75 = new byte[1];






        if (etiTradeNotification == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiTradeNotification.length);

        try {

            array0 = Arrays.copyOfRange(etiTradeNotification, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiTradeNotification, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array20 = Arrays.copyOfRange(etiTradeNotification, 112, 120);
            OrderID = fromByteArraytoLong(array20);

            array22 = Arrays.copyOfRange(etiTradeNotification, 128, 136);
            long ClOrdID = fromByteArraytoLong(array22);

            array23 = Arrays.copyOfRange(etiTradeNotification, 136, 144);
            ActivityTime = fromByteArraytoLong(array23);

            array28 = Arrays.copyOfRange(etiTradeNotification, 164, 168);
            int TradeID = fromByteArraytoInt(array28);

            array14 = Arrays.copyOfRange(etiTradeNotification, 64, 72);
            long TradePrice = fromByteArraytoLong(array14);

            array35 = Arrays.copyOfRange(etiTradeNotification, 188,192);
            int LeavesQty = fromByteArraytoInt(array35);

            array34 = Arrays.copyOfRange(etiTradeNotification, 192,196);
            int CumQty = fromByteArraytoInt(array34);


            array38 = Arrays.copyOfRange(etiTradeNotification, 204,208);
            int LastQty = fromByteArraytoSignedInt(array38);

            array36 = Arrays.copyOfRange(etiTradeNotification, 196, 200);
            int MarketSegmentID = fromByteArraytoSignedInt(array36);

            array62 = Arrays.copyOfRange(etiTradeNotification, 286,298);
            String UCC = fromByteArrayToStringWithEncode(array62,"US-ASCII");

            System.out.println("Response TemplateID is: " + TemplateID);
            System.out.println("TRADE NOTIFICATION RECEIVED");

            switch (TemplateID) {

                case 10500:
                    System.out.println("Trade ID is: " + TradeID);
                    System.out.println("OrderID is: " + OrderID);
                    System.out.println("ClOrdID is: " + ClOrdID);
                    System.out.println("Filled Qty: " + CumQty);
                    System.out.println("Pending Qty is :" + LeavesQty);
                    System.out.println("Executed Qty is: " + LastQty);
                    System.out.println("Executed Price is: " + TradePrice);
                    System.out.println("UCC is: " + UCC);
                    System.out.println("Activity Time is: " + ActivityTime);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiTradeNotification.length - BodyLen);

    }




    public int getByteArrForBookOrderExecutionResponse (byte[] etiBookOrderExecution) {

        byte[] returnMsg = new byte[1024];
        int BodyLen = 0;
        int TemplateID = 0;

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[2];

        //message response
        byte[] array3 = new byte[8];
        byte[] array4 = new byte[8];
        byte[] array5 = new byte[4];
        byte[] array6 = new byte[2];
        byte[] array7 = new byte[16];
        byte[] array8 = new byte[1];

        byte[] array9 = new byte[1];
        byte[] array10 = new byte[1];
        byte[] array11 = new byte[7];
        byte[] array12 = new byte[8];
        byte[] array13 = new byte[8];

        byte[] array14 = new byte[8];
        byte[] array15 = new byte[8];
        byte[] array16 = new byte[8];
        byte[] array17 = new byte[8];
        byte[] array18 = new byte[8];
        byte[] array19 = new byte[8];

        byte[] array20 = new byte[4];
        byte[] array21 = new byte[4];
        byte[] array22 = new byte[4];

        byte[] array23 = new byte[4];
        byte[] array24 = new byte[4];
        byte[] array25 = new byte[4];
        byte[] array26 = new byte[4];

        byte[] array27 = new byte[2];
        byte[] array28 = new byte[2];

        byte[] array29 = new byte[1];
        byte[] array30 = new byte[1];
        byte[] array31 = new byte[1];
        byte[] array32 = new byte[1];
        byte[] array33 = new byte[1];
        byte[] array34 = new byte[1];
        byte[] array35 = new byte[1];

        byte[] array36 = new byte[2];
        byte[] array37 = new byte[16];
        byte[] array38 = new byte[12];
        byte[] array39 = new byte[12];
        byte[] array40 = new byte[12];
        byte[] array41 = new byte[3];

        byte[] array42 = new byte[8];


        if (etiBookOrderExecution == null)
            return 0;

        System.out.println("Response Message ACTUALLY read is: " + etiBookOrderExecution.length);

        try {

            array0 = Arrays.copyOfRange(etiBookOrderExecution, 0, 4);
            BodyLen = fromByteArraytoInt(array0);

            array1 = Arrays.copyOfRange(etiBookOrderExecution, 4, 6);
            TemplateID = fromByteArraytoInt(array1);

            array12 = Arrays.copyOfRange(etiBookOrderExecution, 56, 64);
            OrderID = fromByteArraytoLong(array12);

            array14 = Arrays.copyOfRange(etiBookOrderExecution, 72, 80);
            long ClOrdID = fromByteArraytoLong(array14);

            array16 = Arrays.copyOfRange(etiBookOrderExecution, 88, 96);
            int SecurityID = fromByteArraytoInt(array16);

            array18 = Arrays.copyOfRange(etiBookOrderExecution, 104, 112);
            ActivityTime = fromByteArraytoLong(array18);

            array24 = Arrays.copyOfRange(etiBookOrderExecution, 136,140);
            int LeavesQty = fromByteArraytoInt(array24);

            array25 = Arrays.copyOfRange(etiBookOrderExecution, 140,144);
            int CumQty = fromByteArraytoInt(array25);

            array26 = Arrays.copyOfRange(etiBookOrderExecution, 144,148);
            int CxlQty = fromByteArraytoInt(array26);

            array28 = Arrays.copyOfRange(etiBookOrderExecution, 150,152);
            int ExecRestatementReason = fromByteArraytoInt(array28);

            array31 = Arrays.copyOfRange(etiBookOrderExecution, 154,155);
            int OrdStatus = fromByteArraytoInt(array31);

            array34 = Arrays.copyOfRange(etiBookOrderExecution, 184,185);
            int NoFills = fromByteArraytoInt(array34);

            array35 = Arrays.copyOfRange(etiBookOrderExecution, 158,1);
            int Side = fromByteArraytoInt(array35);

            array38 = Arrays.copyOfRange(etiBookOrderExecution, 177,189);
            String UCC = fromByteArrayToStringWithEncode(array38,"UTF-8");

            array42 = Arrays.copyOfRange(etiBookOrderExecution, 216,224);
            long Price = fromByteArraytoLong(array42);


            System.out.println("Response TemplateID is: " + TemplateID);

            switch (TemplateID) {

                case 10104:
                    System.out.println("OrderID is: " + OrderID);
                    System.out.println("ClOrdID is: " + ClOrdID);
                    System.out.println("SecurityID is :" + SecurityID);
                    System.out.println("Filled Qty: " + CumQty);
                    System.out.println("Pending Qty is :" + LeavesQty);
                    System.out.println("Cancelled Qty is :" + CxlQty);
                    System.out.println("Execution Status is: " + ExecRestatementReason);
                    System.out.println("Order Status is: " + OrdStatus);
                    System.out.println("Client Code is: " + UCC);
                    System.out.println("No of fills is: " + NoFills);
                    System.out.println("Side is: " + Side);
                    System.out.println("Price is:" + Price);
                    System.out.println("Activity Time is: " + ActivityTime);
                    break;

                default:
                    System.out.println("TemplateID is: " + TemplateID);
                    break;
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("BODYLEN of " + TemplateID + " is: " + BodyLen);
        return (etiBookOrderExecution.length - BodyLen);

    }




}
