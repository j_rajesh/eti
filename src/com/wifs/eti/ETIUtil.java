package com.wifs.eti;



import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import org.apache.log4j.Logger;

public class ETIUtil {

    private static Logger logger = Logger.getLogger(ETIUtil.class);

    public static byte[] merge(final byte[]... arrays) {
        int size = 0;
        for (byte[] a : arrays)
            size += a.length;

        byte[] res = new byte[size];

        int destPos = 0;
        for (int i = 0; i < arrays.length; i++) {
            if (i > 0) destPos += arrays[i - 1].length;
            int length = arrays[i].length;
            System.arraycopy(arrays[i], 0, res, destPos, length);
        }

        return res;
    }


    public static int fromByteArraytoInt(byte[] bytes) {

        int value = 0;

        for (int i = 0; i < bytes.length; i++) {
            value += ((int) bytes[i] & 0xffL) << (8 * i);
        }
        return value;
    }

    // for eti response signed eti

    public static int fromByteArraytoSignedInt(byte[] bytes) {

        int value = 0;
        int size = bytes.length;

        for (int i = 0; i < size; i++) {
            value += ((int) bytes[i] & 0xffL) << (8 * i);
        }

        value += ((int) bytes[size - 1] ) << ( 8 * (size -1));
        return value;
    }

    // for eti response timestamp

    public static String fromByteArraytoTimeStamp(byte[] bytes) {

        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("IST"));

        long sTime = fromByteArraytoLong(bytes);
        String sTimeStr = sdf.format(new Date(Long.valueOf(sTime/1000000)));
        return sTimeStr;

    }

    //for eti connection gateway response

    public static String fromByteArraytoIPAddress(byte[] bytes) {

        String str = "";
        String s1="";

        int b = 0;
        String bstr = "";

        try {
            for (int i = 0; i < bytes.length; i++) {

                b = (int) (bytes[i] & 0xff);
                bstr = String.valueOf(b);
                str = i==0 ? bstr : bstr + "." + str;

            }

            System.out.println("IP Address is : " + str);

        } catch (Exception e) {
            e.printStackTrace();
            //logger.error(e.toString());
        }

        return str;
    }

    public static short fromByteArraytoShort(byte[] bytes) {

        short value = 0;

        for (int i = 0; i < bytes.length; i++) {
            value += ((short) bytes[i] & 0xffL) << (8 * i);
        }
        return value;
    }

    public static long bytesToLong(byte[] bytes) {

        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(bytes, 0, bytes.length);
        buffer.flip();//need flip
        return buffer.getLong();
    }

    public static long fromByteArraytoLong(byte[] bytes) {

        long value = 0;

        for (int i = 0; i < bytes.length; i++) {
            value += ((long) bytes[i] & 0xffL) << (8 * i);
        }
        return value;
    }

    public static String fromByteArrayToString(byte[] bytes) {
        String s = bytes.toString();
        return s;
    }

    public static String fromByteArrayToStringWithEncode(byte[] bytes, String encoding) {

        String s = null;

        try {
            s = new String(bytes, encoding);
        } catch (Exception e) {

        }

        return s;
    }


    public static byte[] initializeByteArraySignedInt(int size) {

        byte[] bytes = new byte[size];

        try {
            for (int i = 0; i < size - 1; i++) {
                bytes[i] = (byte) (0x00);
            }

            bytes[size - 1] = (byte) (0x80);

        } catch (Exception e) {
            e.printStackTrace();
            //logger.error(e.toString());
        }

        return bytes;
    }


    public static byte[] initializeByteArray(int size) {

        byte[] bytes = new byte[size];

        try {
            for (int i = 0; i < size; i++) {
                bytes[i] = (byte) (0xff);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //logger.error(e.toString());
        }

        return bytes;
    }



    public static byte[] getByteArray(int size, int value) {

        byte[] bytes = new byte[size];

        try {
            for (int i = 0; i < size; i++) {
                bytes[i] = (byte) ((value >>> (i * 8)) & 0xff);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //logger.error(e.toString());
        }

        return bytes;
    }


    //for eti

    public static byte[] getByteArrayfromLong(int size, long value) {

        byte[] bytes = new byte[size];

        try {
            for (int i = 0; i < size; i++) {
                bytes[i] = (byte) ((value >>> (i * 8)) & 0xff);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //logger.error(e.toString());
        }

        return bytes;
    }


    public static byte[] getByteArraySignedLong(int size, long value) {

        byte[] bytes = new byte[size];

        try {
            for (int i = 0; i < size - 1; i++) {
                bytes[i] = (byte) ((value >>> (i * 8)) & 0xff);
            }

            bytes[size - 1] = (byte) ((value >>> ( (size - 1) * 8)) | 0x80); //signed bit preserved

        } catch (Exception e) {
            e.printStackTrace();
            //logger.error(e.toString());
        }

        return bytes;
    }


    //for eti

    public static byte[] getByteArraySignedInt(int size, int value) {

        byte[] bytes = new byte[size];

        try {
            for (int i = 0; i < size - 1; i++) {
                    bytes[i] = (byte) ((value >>> (i * 8)) & 0xff);
            }

            bytes[size - 1] = (byte) ((value >>> ( (size - 1) * 8))); //signed bit preserved

        } catch (Exception e) {
            e.printStackTrace();
            //logger.error(e.toString());
        }

        return bytes;
    }


    public static byte[] stringToBytesASCII(String str, int length) {
        char[] buffer = str.toCharArray();
        byte[] b = new byte[length];
        for (int i = 0; i < length; i++) {
            if (i < buffer.length)
                b[i] = (byte) buffer[i];
            else
                b[i] = 0;
        }

        return b;
    }
}


//ETIUtil etiUtil = new ETIUtil();

