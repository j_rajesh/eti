package com.wifs.eti;

import java.util.Arrays;

import static  com.wifs.eti.ETIUtil.*;


public class ETIRequest {

    //TODO: Initialize methods for types: uint, int, ulong, long, string, char

    public static byte[] getByteArrForNewOrderSingleRequest(String etiNewOrderSingleMessage) {

        byte[] returnMsg = new byte[216];

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[8];
        byte[] array3 = new byte[2];

        //request header
        byte[] array4 = new byte[4];
        byte[] array5 = new byte[4];

        //message body
        byte[] array6 = new byte[8];
        byte[] array7 = new byte[8];

        byte[] array8 = new byte[8];
        byte[] array9 = new byte[8];
        byte[] array10 = new byte[8];
        byte[] array11 = new byte[8];

        byte[] array12 = new byte[4];
        byte[] array13 = new byte[4];

        byte[] array14 = new byte[4];
        byte[] array15 = new byte[4];
        byte[] array16 = new byte[4];
        byte[] array17 = new byte[4];

        byte[] array18 = new byte[4];
        byte[] array19 = new byte[4];
        byte[] array20 = new byte[4];

        byte[] array21 = new byte[5];
        byte[] array22 = new byte[7];

        byte[] array23 = new byte[9];
        byte[] array24 = new byte[1];

        byte[] array25 = new byte[1];
        byte[] array26 = new byte[1];
        byte[] array27 = new byte[1];
        byte[] array28 = new byte[1];
        byte[] array29 = new byte[1];
        byte[] array30 = new byte[1];
        byte[] array31 = new byte[1];

        byte[] array32 = new byte[1];
        byte[] array33 = new byte[2];
        byte[] array34 = new byte[1];
        byte[] array35 = new byte[2];
        byte[] array36 = new byte[1];

        byte[] array37 = new byte[20];

        byte[] array38 = new byte[16];
        byte[] array39 = new byte[12];
        byte[] array40 = new byte[12];
        byte[] array41 = new byte[12];


        try {

            System.out.println("New Order Request is: " + etiNewOrderSingleMessage);

            String[] messeArray = etiNewOrderSingleMessage.split("\\|");

            System.out.println("New Order Request AFTER | split is: " + messeArray.length + "," + messeArray[25].toString() + "," + messeArray[39].toString());

            //Initialize fields
            //TODO: To initialize in another switch case

            array6 = initializeByteArraySignedInt(8);
            array7 = initializeByteArraySignedInt(8);
            array17 =  initializeByteArray(4);
            array18 =  initializeByteArraySignedInt(4);
            array31 = initializeByteArray(1);

            for (int i = 0; i < messeArray.length; i++) {
                if (messeArray[i] != null && !"".equals(messeArray[i])) {

                    switch (i) {

                        case 0:
                            array0 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 1:
                            array1 = getByteArray(2, Integer.parseInt(messeArray[i]));
                            break;
                        case 2:
                            array2 = stringToBytesASCII(messeArray[i], 8);
                            break;
                        case 3:
                            array3 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 4:
                            array4 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 5:
                            array5 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 6:
                            array6 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 7:
                            array7 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 8:
                            array8 = getByteArray(8, Integer.parseInt(messeArray[i]));
                            break;
                        case 9:
                            array9 = getByteArrayfromLong(8, Long.parseLong((messeArray[i])));
                            break;
                        case 10:
                            array10 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 11:
                            array11 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 12:
                            array12 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 13:
                            array13 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 14:
                            array14 = getByteArraySignedInt(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 15:
                            array15 = getByteArraySignedInt(4, Integer.parseInt(messeArray[i])); //Qty
                            break;
                        case 16:
                            array16 = getByteArraySignedInt(4, Integer.parseInt(messeArray[i])); //Qty
                            break;
                        case 17:
                            array17 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 18:
                            array18 = getByteArraySignedInt(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 19:
                            array19 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 20:
                            array20 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 21:
                            array21 = stringToBytesASCII(messeArray[i], 5);
                            break;
                        case 22:
                            array22 = stringToBytesASCII(messeArray[i], 7);
                            break;
                        case 23:
                            array23 = stringToBytesASCII(messeArray[i], 9);
                            break;
                        case 24:
                            array24 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;

                        case 25:
                            array25 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 26:
                            array26 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 27:
                            array27 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 28:
                            array28 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 29:
                            array29 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 30:
                            array30 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 31:
                            array31 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;

                        case 32:
                            array32 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 33:
                            array33 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 34:
                            array34 = stringToBytesASCII(messeArray[i], 1);
                            break;
                        case 35:
                            System.out.println("Here at: " + i + "," + messeArray[i].toString());
                            array35 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 36:
                            array36 = stringToBytesASCII(messeArray[i], 1);
                            break;

                        case 37:
                            array37 = stringToBytesASCII(messeArray[i], 20);
                            break;
                        case 38:
                            array38 = stringToBytesASCII(messeArray[i], 16);
                            break;
                        case 39:
                            System.out.println("Here at: " + i + messeArray[i].toString());
                            array39 = stringToBytesASCII(messeArray[i], 12);
                            break;
                        case 40:
                            array40 = stringToBytesASCII(messeArray[i], 12);
                            break;
                        case 41:
                            System.out.println("Here at: " + i + messeArray[i].toString());
                            array41 = stringToBytesASCII(messeArray[i], 12);
                            break;
                    }
                }
            }


            returnMsg = merge ( array0, array1, array2, array3, array4, array5,
                    array6, array7, array8, array9, array10,
                    array11, array12, array13, array14, array15, array16, array17,
                    array18, array19, array20,
                    array21, array22, array23, array24, array25, array26, array27,
                    array28, array29, array30,
                    array31, array32, array33, array34, array35, array36, array37, array38, array39, array40, array41);


        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("New Order Request in BYTES is: " + Arrays.toString(returnMsg) + "," + returnMsg.length);

        return returnMsg;

    }


    public static byte[] getByteArrForSubscribeRequest(String etiSubscribeMessage) {

        byte[] returnMsg = new byte[32];

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[8];
        byte[] array3 = new byte[2];

        //request header
        byte[] array4 = new byte[4];
        byte[] array5 = new byte[4];

        //message body
        byte[] array6 = new byte[4];
        byte[] array7 = new byte[1];
        byte[] array8 = new byte[3];

        try {

            System.out.println("Subcribe Request is: " + etiSubscribeMessage);

            String[] messeArray = etiSubscribeMessage.split("\\|");

            for (int i = 0; i < messeArray.length; i++) {
                if (messeArray[i] != null && !"".equals(messeArray[i])) {

                    switch (i) {

                        case 0:
                            array0 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 1:
                            array1 = getByteArray(2, Integer.parseInt(messeArray[i]));
                            break;
                        case 2:
                            array2 = stringToBytesASCII(messeArray[i], 8);
                            break;
                        case 3:
                            array3 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 4:
                            array4 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 5:
                            array5 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 6:
                            array6 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 7:
                            array7 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 8:
                            array8 = stringToBytesASCII(messeArray[i],3);
                            break;
                    }
                }
            }

            returnMsg = merge ( array0, array1, array2, array3, array4, array5,
                    array6, array7, array8);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnMsg;

    }





    public static byte[] getByteArrForSessionLogonRequest(String etiSessionLogonMessage) {

        byte[] returnMsg = new byte[280];

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[8];
        byte[] array3 = new byte[2];

        //request header
        byte[] array4 = new byte[4];
        byte[] array5 = new byte[4];

        //message body
        byte[] array6 = new byte[4];
        byte[] array7 = new byte[4];
        byte[] array8 = new byte[30];
        byte[] array9 = new byte[32];
        byte[] array10 = new byte[1];
        byte[] array11 = new byte[1];
        byte[] array12 = new byte[1];
        byte[] array13 = new byte[30];
        byte[] array14 = new byte[30];
        byte[] array15 = new byte[30];
        byte[] array16 = new byte[30];
        byte[] array17 = new byte[30];
        byte[] array18 = new byte[30];
        byte[] array19 = new byte[3];


        try {

            String[] messeArray = etiSessionLogonMessage.split("\\|");

            for (int i = 0; i < messeArray.length; i++) {
                if (messeArray[i] != null && !"".equals(messeArray[i])) {

                    switch (i) {

                        case 0:
                            array0 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 1:
                            array1 = getByteArray(2, Integer.parseInt(messeArray[i]));
                            break;
                        case 2:
                            array2 = stringToBytesASCII(messeArray[i], 8);
                            break;
                        case 3:
                            array3 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 4:
                            array4 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 5:
                            array5 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 6:
                            array6 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            ;
                            break;
                        case 7:
                            array7 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 8:
                            array8 = stringToBytesASCII(messeArray[i], 30);
                            break;
                        case 9:
                            array9 = stringToBytesASCII(messeArray[i], 32);
                            break;
                        case 10:
                            array10 = stringToBytesASCII(messeArray[i], 1);
                            break;
                        case 11:
                            array11 = stringToBytesASCII(messeArray[i], 1);
                            break;
                        case 12:
                            array12 = stringToBytesASCII(messeArray[i], 1);
                            break;
                        case 13:
                            array13 = stringToBytesASCII(messeArray[i], 30);
                            break;
                        case 14:
                            array14 = stringToBytesASCII(messeArray[i], 30);
                            break;
                        case 15:
                            array15 = stringToBytesASCII(messeArray[i], 30);
                            break;
                        case 16:
                            array16 = stringToBytesASCII(messeArray[i], 30);
                            break;
                        case 17:
                            array17 = stringToBytesASCII(messeArray[i], 30);
                            break;
                        case 18:
                            array18 = stringToBytesASCII(messeArray[i], 30);
                            break;
                        case 19:
                            array19 = stringToBytesASCII(messeArray[i], 3);
                            break;
                    }
                }
            }

            returnMsg = merge ( array0, array1, array2, array3, array4, array5,
                    array6, array7, array8, array9, array10, array11,
                    array12, array13, array14, array15, array16, array17,
                    array18, array19 );


        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnMsg;

    }



    public static byte[] getByteArrForReplaceOrderSingleRequest(String etiSessionReplaceOrderMessage) {

        byte[] returnMsg = new byte[248];

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[8];
        byte[] array3 = new byte[2];

        //request header
        byte[] array4 = new byte[4];
        byte[] array5 = new byte[4];

        //message body
        byte[] array6 = new byte[8];

        byte[] array7 = new byte[8];
        byte[] array8 = new byte[8];
        byte[] array9 = new byte[8];
        byte[] array10 = new byte[8];
        byte[] array11 = new byte[8];
        byte[] array12 = new byte[8];
        byte[] array13 = new byte[8];
        byte[] array14 = new byte[8];

        byte[] array15 = new byte[4];
        byte[] array16 = new byte[4];
        byte[] array17 = new byte[4];
        byte[] array18 = new byte[4];
        byte[] array19 = new byte[4];
        byte[] array20 = new byte[4];
        byte[] array21 = new byte[4];
        byte[] array22 = new byte[4];
        byte[] array23 = new byte[4];
        byte[] array24 = new byte[4];

        byte[] array25 = new byte[5];
        byte[] array26 = new byte[7];
        byte[] array27 = new byte[9];

        byte[] array28 = new byte[1];
        byte[] array29 = new byte[1];
        byte[] array30 = new byte[1];
        byte[] array31 = new byte[1];
        byte[] array32 = new byte[1];
        byte[] array33 = new byte[1];
        byte[] array34 = new byte[1];
        byte[] array35 = new byte[1];
        byte[] array36 = new byte[1];
        byte[] array37 = new byte[1];

        byte[] array38 = new byte[2];
        byte[] array39 = new byte[1];
        byte[] array40 = new byte[2];
        byte[] array41 = new byte[1];

        byte[] array42 = new byte[20];
        byte[] array43 = new byte[16];

        byte[] array44 = new byte[12];
        byte[] array45 = new byte[12];
        byte[] array46 = new byte[12];
        byte[] array47 = new byte[3];


        try {

            System.out.println("Replace Order Request is: " + etiSessionReplaceOrderMessage);

            String[] messeArray = etiSessionReplaceOrderMessage.split("\\|");

            System.out.println("etiReplaceOrderMessage split is: " + etiSessionReplaceOrderMessage.length() + "," + messeArray.length);


            for (int i = 0; i < messeArray.length; i++) {
                if (messeArray[i] != null && !"".equals(messeArray[i])) {

                    switch (i) {

                        case 0:
                            array0 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 1:
                            array1 = getByteArray(2, Integer.parseInt(messeArray[i]));
                            break;
                        case 2:
                            array2 = stringToBytesASCII(messeArray[i], 8);
                            break;
                        case 3:
                            array3 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 4:
                            array4 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 5:
                            array5 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 6:
                            array6 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 7:
                            array7 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 8:
                            array8 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 9:
                            array9 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 10:
                            array10 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 11:
                            array11 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 12:
                            array12 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 13:
                            array13 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            System.out.println("Activity Time");
                            break;
                        case 14:
                            array14 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 15:
                            array15 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 16:
                            array16 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 17:
                            array17 = getByteArraySignedInt(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 18:
                            array18 = getByteArraySignedInt(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 19:
                            array19 = getByteArraySignedInt(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 20:
                            array20 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 21:
                            array21 = getByteArraySignedInt(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 22:
                            array22 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 23:
                            array23 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 24:
                            array24 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 25:
                            array25 = stringToBytesASCII(messeArray[i], 5);
                            break;
                        case 26:
                            array26 = stringToBytesASCII(messeArray[i], 7);
                            break;
                        case 27:
                            array27 = stringToBytesASCII(messeArray[i], 9);
                            break;
                        case 28:
                            array28 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 29:
                            array29 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 30:
                            array30 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 31:
                            array31 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 32:
                            array32 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 33:
                            array33 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 34:
                            array34 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 35:
                            array35 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 36:
                            array36 = getByteArray(1, Integer.parseInt(messeArray[i]));
                            break;
                        case 37:
                            array37 = stringToBytesASCII(messeArray[i], 1);
                            break;
                        case 38:
                            array38 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 39:
                            array39 = stringToBytesASCII(messeArray[i], 1);
                            break;
                        case 40:
                            array40 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 41:
                            array41 = stringToBytesASCII(messeArray[i], 1);
                            break;
                        case 42:
                            array42 = stringToBytesASCII(messeArray[i], 20);
                            break;
                        case 43:
                            array43 = stringToBytesASCII(messeArray[i], 16);
                            break;
                        case 44:
                            array44 = stringToBytesASCII(messeArray[i], 12);
                            System.out.println("Modify Orders - UCC");
                            break;
                        case 45:
                            array45 = stringToBytesASCII(messeArray[i], 12);
                            break;
                        case 46:
                            array46 = stringToBytesASCII(messeArray[i], 12);
                            break;
                        case 47:
                            array47 = stringToBytesASCII(messeArray[i], 3);
                            break;
                    }
                }
            }

            array10 =  initializeByteArraySignedInt(8);
            array20 =  initializeByteArray(4);
            array21 =  initializeByteArraySignedInt(4);

            returnMsg = merge ( array0, array1, array2, array3, array4, array5,
                    array6, array7, array8, array9, array10,
                    array11, array12, array13, array14, array15, array16, array17, array18, array19, array20,
                    array21, array22, array23, array24, array25, array26, array27, array28, array29, array30,
                    array31, array32, array33, array34, array35, array36, array37, array38, array39, array40,
                    array41, array42, array43, array44, array45, array46, array47);



        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnMsg;

    }




    public static byte[] getByteArrForSessionLogoutRequest(String etiSessionLogoutMessage) {

        byte[] returnMsg = new byte[24];

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[8];
        byte[] array3 = new byte[2];

        //request header
        byte[] array4 = new byte[4];
        byte[] array5 = new byte[4];

        try {

            String[] messeArray = etiSessionLogoutMessage.split("\\|");

            for (int i = 0; i < messeArray.length; i++) {
                if (messeArray[i] != null && !"".equals(messeArray[i])) {

                    switch (i) {

                        case 0:
                            array0 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 1:
                            array1 = getByteArray(2, Integer.parseInt(messeArray[i]));
                            break;
                        case 2:
                            array2 = stringToBytesASCII(messeArray[i], 8);
                            break;
                        case 3:
                            array3 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 4:
                            array4 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 5:
                            array5 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                    }
                }
            }

            returnMsg = merge ( array0, array1, array2, array3, array4, array5);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnMsg;

    }


    public static byte[] getByteArrForUserLogonRequest(String etiUserLogonMessage) {

        byte[] returnMsg = new byte[64];

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[8];
        byte[] array3 = new byte[2];

        //request header
        byte[] array4 = new byte[4];
        byte[] array5 = new byte[4];

        //message body
        byte[] array6 = new byte[4];
        byte[] array7 = new byte[32];
        byte[] array8 = new byte[4];

        try {

            System.out.println("User logon: " + etiUserLogonMessage);

            String[] messeArray = etiUserLogonMessage.split("\\|");

            for (int i = 0; i < messeArray.length; i++) {
                if (messeArray[i] != null && !"".equals(messeArray[i])) {

                    switch (i) {

                        case 0:
                            array0 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 1:
                            array1 = getByteArray(2, Integer.parseInt(messeArray[i]));
                            break;
                        case 2:
                            array2 = stringToBytesASCII(messeArray[i], 8);
                            break;
                        case 3:
                            array3 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 4:
                            array4 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 5:
                            array5 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 6:
                            array6 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 7:
                            array7 = stringToBytesASCII(messeArray[i], 32);
                            break;
                        case 8:
                            array8 = stringToBytesASCII(messeArray[i], 4);
                            break;
                    }
                }
            }

            returnMsg = merge ( array0, array1, array2, array3, array4, array5,
                    array6, array7, array8);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnMsg;

    }


    public static byte[] getByteArrForCancelOrderSingleRequest(String etiCancelOrderSingleMessage) {

        byte[] returnMsg = new byte[96];

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[8];
        byte[] array3 = new byte[2];

        //request header
        byte[] array4 = new byte[4];
        byte[] array5 = new byte[4];

        //message body
        byte[] array6 = new byte[8];
        byte[] array7 = new byte[8];
        byte[] array8 = new byte[8];
        byte[] array9 = new byte[8];

        byte[] array10 = new byte[4];
        byte[] array11 = new byte[4];
        byte[] array12 = new byte[4];
        byte[] array13 = new byte[4];
        byte[] array14 = new byte[4];

        byte[] array15 = new byte[16];
        byte[] array16 = new byte[4];


        try {

            System.out.println("Cancel message to send is: " + etiCancelOrderSingleMessage);

            String[] messeArray = etiCancelOrderSingleMessage.split("\\|");

            array11 = initializeByteArraySignedInt(4);

            for (int i = 0; i < messeArray.length; i++) {
                if (messeArray[i] != null && !"".equals(messeArray[i])) {

                    switch (i) {

                        case 0:
                            array0 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 1:
                            array1 = getByteArray(2, Integer.parseInt(messeArray[i]));
                            break;
                        case 2:
                            array2 = stringToBytesASCII(messeArray[i], 8);
                            break;
                        case 3:
                            array3 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 4:
                            array4 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 5:
                            array5 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 6:
                            array6 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 7:
                            array7 = getByteArray(8, Integer.parseInt(messeArray[i]));
                            break;
                        case 8:
                            array8 = getByteArray(8, Integer.parseInt(messeArray[i]));
                            break;
                        case 9:
                            array9 = getByteArrayfromLong(8, Long.parseLong(messeArray[i]));
                            break;
                        case 10:
                            array10 = getByteArraySignedInt(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 11:
                            array11 = getByteArraySignedInt(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 12:
                            array12 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 13:
                            array13 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 14:
                            array14 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 15:
                            array15 = stringToBytesASCII(messeArray[i], 16);
                            break;
                        case 16:
                            array16 = stringToBytesASCII(messeArray[i], 4);
                            break;
                    }
                }
            }

            returnMsg = merge ( array0, array1, array2, array3, array4, array5,
                    array6, array7, array8, array9, array10, array11, array12, array13, array14, array15, array16);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnMsg;

    }



    public static byte[] getByteArrForUserLogoutRequest(String etiUserLogoutMessage) {

        byte[] returnMsg = new byte[32];

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[8];
        byte[] array3 = new byte[2];

        //request header
        byte[] array4 = new byte[4];
        byte[] array5 = new byte[4];

        //message body
        byte[] array6 = new byte[4];
        byte[] array7 = new byte[4];

        try {

            String[] messeArray = etiUserLogoutMessage.split("\\|");

            for (int i = 0; i < messeArray.length; i++) {
                if (messeArray[i] != null && !"".equals(messeArray[i])) {

                    switch (i) {

                        case 0:
                            array0 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 1:
                            array1 = getByteArray(2, Integer.parseInt(messeArray[i]));
                            break;
                        case 2:
                            array2 = stringToBytesASCII(messeArray[i], 8);
                            break;
                        case 3:
                            array3 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 4:
                            array4 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 5:
                            array5 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 6:
                            array6 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 7:
                            array7 = stringToBytesASCII(messeArray[i], 4);
                            break;
                    }
                }
            }

            returnMsg = merge ( array0, array1, array2, array3, array4, array5,
                    array6, array7);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnMsg;

    }



    public static byte[] getByteArrForSessionHeartbeat(String etiSessionHeartbeat) {
        byte[] returnMsg = new byte[16];

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[8];
        byte[] array3 = new byte[2];

        try {

            String[] messeArray = etiSessionHeartbeat.split("\\|");

            for (int i = 0; i < messeArray.length; i++) {
                if (messeArray[i] != null && !"".equals(messeArray[i])) {

                    switch (i) {

                        case 0:
                            array0 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 1:
                            array1 = getByteArray(2, Integer.parseInt(messeArray[i]));
                            break;
                        case 2:
                            array2 = stringToBytesASCII(messeArray[i], 8);
                            break;
                        case 3:
                            array3 = stringToBytesASCII(messeArray[i], 2);
                            break;
                    }
                }
            }

            returnMsg = merge ( array0, array1, array2, array3);


        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnMsg;

    }



    public static byte[] getByteArrForConnectionGatewayRequest(String etiConnectionGatewayMessage) {

        byte[] returnMsg = new byte[96];

        //message header
        byte[] array0 = new byte[4];
        byte[] array1 = new byte[2];
        byte[] array2 = new byte[8];
        byte[] array3 = new byte[2];

        //request header
        byte[] array4 = new byte[4];
        byte[] array5 = new byte[4];

        //message body
        byte[] array6 = new byte[4];
        byte[] array7 = new byte[30];
        byte[] array8 = new byte[32];
        byte[] array9 = new byte[6];

        try {
            String[] messeArray = etiConnectionGatewayMessage.split("\\|");

            System.out.println("Entering Connection Gateway with Request");

            for (int i = 0; i < messeArray.length; i++) {
                if (messeArray[i] != null && !"".equals(messeArray[i])) {

                    switch (i) {

                        case 0:
                            array0 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 1:
                            array1 = getByteArray(2, Integer.parseInt(messeArray[i]));
                            break;
                        case 2:
                            array2 = stringToBytesASCII(messeArray[i], 8);
                            break;
                        case 3:
                            array3 = stringToBytesASCII(messeArray[i], 2);
                            break;
                        case 4:
                            array4 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 5:
                            array5 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 6:
                            array6 = getByteArray(4, Integer.parseInt(messeArray[i]));
                            break;
                        case 7:
                            array7 = stringToBytesASCII(messeArray[i], 30);
                            break;
                        case 8:
                            array8 = stringToBytesASCII(messeArray[i], 32);
                            break;
                        case 9:
                            array9 = stringToBytesASCII(messeArray[i], 6);
                            break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Converted Gateway Connection Message");

        returnMsg = merge ( array0, array1, array2, array3, array4, array5,
                array6, array7, array8, array9);

        return returnMsg;

    }

}
