package com.wifs.eti;

import org.apache.log4j.BasicConfigurator;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

/*

    FUNDSINDIA ETI SERVER
    ----------------------

    1. REQUEST MESSAGES,
    2. RESPONSE MESSAGES,
    3. ADMIN NOTIFICATIONS,
    4. ORDER TRANSACTIONS,
    5. ORDER NOTIFICATIONS,
    6. TRADE NOTIFICATIONS


    1. Initiate connection with Connection Gateway to get BSE SERVER IP:Port
    2. Initiate connection with Transaction server with Session Logon Request
    3. Receive and Maintain Heartbeat Specifics
    4. Authenticate with User Logon Request
    5. Initiate Transactions
    6. Confirm Transactions
    7. Listen for Admin Notification Messages
    8. Listen for Transaction Notification Messages
    9. User Logout to disconnect
    10. Session Logout to disconnect

 */


public class Main {

    public static void main(String[] args) {

        try {

            System.out.println("Hello ETI");

            //Keep log4j tied to System.out
            BasicConfigurator.configure();

            //ETI Session Object
            ETISession etiSession = new ETISession();

            //ETI Data conversion Object
            ETIRequest etiRequest = etiSession.getEtiRequest();
            ETIResponse etiResponse = etiSession.getEtiResponse();

            //ETI Thread to listen for response
            ETIThread etiThread = new ETIThread(etiSession);


            //Contact Connection Gateway
            if (etiSession.createConnection()) {

                System.out.println("ETI Connection Gateway Request made successfully");

                //HF Session
               //// byte[] connectionGatewayRequest = etiRequest.getByteArrForConnectionGatewayRequest("96|10020|||1||708601002|2.2|bse12345|");
                //LF Session
                //byte[] connectionGatewayRequest = etiRequest.getByteArrForConnectionGatewayRequest("96|10020|||1||652101004|2.2|bse12345|");
                byte[] connectionGatewayRequest = etiRequest.getByteArrForConnectionGatewayRequest("96|10020|||1||708601002|2.2|bse12345|");
                etiSession.sendMessage(connectionGatewayRequest);

            /*
            //Parse the Connection Gateway Response
            byte[] connectionGatewayConnectionResponse = etiResponse.getByteArrForConnectionGatewayResponse(etiSession.receiveMessage());

            if (connectionGatewayConnectionResponse.length > 0) {
                System.out.println("ETI - Connection Gateway Response received: " + connectionGatewayConnectionResponse.length);
            }
            */

            }

            etiThread.start();

            TimeUnit.SECONDS.sleep(5);

            //Contact the Exchange Server host with port

            if (etiSession.createConnection()) {

                System.out.println("ETI Session Logon Request");
                System.out.println("---------------------------------------");
                //HF Session
                ////byte[] sessionLogonRequest = etiRequest.getByteArrForSessionLogonRequest("280|10000|||1|||708601002|2.2|bse12345|M|N|N|FIETI|1.0|FIH|FIS|1.0|FI|");
                //LFSession
                //byte[] sessionLogonRequest = etiRequest.getByteArrForSessionLogonRequest("280|10000|||1|||652101004|2.2|bse12345|M|N|N|FIETI|1.0|FIH|FIS|1.0|FI|");
                byte[] sessionLogonRequest = etiRequest.getByteArrForSessionLogonRequest("280|10000|||1|||708601002|2.2|bse12345|M|N|N|FIETI|1.0|FIH|FIS|1.0|FI|");
                etiSession.sendMessage(sessionLogonRequest);
            }



            TimeUnit.SECONDS.sleep(5);


            //Make a Hearbeat Request
            System.out.println("ETI Session Heartbeat");
            System.out.println("---------------------------------------");
            byte[] sessionHeartbeatRequest = etiRequest.getByteArrForSessionHeartbeat("16|10011||");
            etiSession.sendMessage(sessionHeartbeatRequest);


            /*
            //Parse Hearbeat Notification, if available
            byte[] sessionHeartbeatResponse = etiResponse.getByteArrForSessionHeartbeatNotification(etiSession.receiveMessage());
            if (sessionHeartbeatResponse != null) {
                System.out.println("ETI - Session Heartbeat Response received: " + sessionHeartbeatResponse.length);
            }
            */

            TimeUnit.SECONDS.sleep(5);

            //Make a User Logon Request
            System.out.println("ETI User Logon");
            System.out.println("---------------------------------------");
            byte[] userLogonRequest = etiRequest.getByteArrForUserLogonRequest("64|10018|||2||708601002|bse12345|");
            ///byte[] userLogonRequest = etiRequest.getByteArrForUserLogonRequest("64|10018|||1||652101004|bse12345|");
            etiSession.sendMessage(userLogonRequest);

            TimeUnit.SECONDS.sleep(5);


            //Make a Subscribe Message
            System.out.println("ETI Subscribe to Trade");
            System.out.println("---------------------------------------");
            //byte[] subscribeMessageTrade = etiRequest.getByteArrForSubscribeRequest("32|10025|||3||652101004|1|");
            byte[] subscribeMessageTrade = etiRequest.getByteArrForSubscribeRequest("32|10025|||3||708601002|1|");

            etiSession.sendMessage(subscribeMessageTrade);

            TimeUnit.SECONDS.sleep(5);



            /*
            System.out.println("ETI Subscribe to Listener Data");
               System.out.println("---------------------------------------");
            byte[] subscribeMessageLData = etiRequest.getByteArrForSubscribeRequest("32|10025|||4||708601002|5|");
            etiSession.sendMessage(subscribeMessageLData);

            TimeUnit.SECONDS.sleep(5);
            */


            /*
            //Parse the User Logon Response
            byte[] userLogonResponse = etiResponse.getByteArrForUserLogonResponse(etiSession.receiveMessage());

            if (userLogonResponse != null) {
                System.out.println("ETI - Session Logon Response received: " + userLogonResponse.length);
            }
            */



            TimeUnit.SECONDS.sleep(5);

            //TODO: Use exact locationid format as per specifications


            //Make a Single Order Request
            System.out.println("ETI Transaction - New Order");
            System.out.println("---------------------------------------");

            ////STANDARD ORDERS
            //MARKET ORDER
            //byte[] newOrderRequest = etiRequest.getByteArrForNewOrderSingleRequest("216|10100|||4|708601002|||0|1111111111111088|||||999|50|50|||532540|||||30|1|1|5|0|0|1|255|1|A1|C|||||WIA999999|||");

            //LIMIT ORDER
            byte[] newOrderRequest = etiRequest.getByteArrForNewOrderSingleRequest("216|10100|||4|708601002|206600000000|||1111111111111088|||||999|500|500|||532540|||||30|1|1|2|0|0|2|255|1|A1|C|||||WIA999999|||");

            //STOP LIMIT ORDER
            //byte[] newOrderRequest = etiRequest.getByteArrForNewOrderSingleRequest("216|10100|||4|708601002|||0|1111111111111088|||||999|50|50|||532540|||||30|1|1|5|0|0|1|255|1|A1|C|||||WIA999999|||");



            //MARKET ORDER
            //byte[] newOrderRequest = etiRequest.getByteArrForNewOrderSingleRequest("216|10100|||3|708601002|||0|1111111111111088|||||999|50|50|||532540|||||30|0|1|5|0|0|2|255|1|A1|C|||||123|||");
            // byte[] newOrderRequest = etiRequest.getByteArrForNewOrderSingleRequest("216|10100|||4|708601002|||0|1111111111111088|||||999|50|50|||500112|||||30|0|1|5|0|0|2|255|1|A1|C|||||WIA999999|||");

            //byte[] newOrderRequest = etiRequest.getByteArrForNewOrderSingleRequest("216|10100|||4|708601002|||0|1111111111111088|||||999|50|50|||532540|||||30|0|1|5|0|0|2|255|1|A1|C|||||WIA999999|||");
            ////byte[] newOrderRequest = etiRequest.getByteArrForNewOrderSingleRequest("216|10100|||4|652101004|||0|1111111111111088|||||999|50|50|||532540|||||30|0|1|5|0|0|2|255|1|A1|C|||||WIA999999|||");


            //LIMIT ORDER - lEAN
            //byte[] newOrderRequest = etiRequest.getByteArrForNewOrderSingleRequest("216|10100|||4|708601002|204950000000|||1111111111111088|||||999|50|50|||532540|||||30|0|1|2|0|0|2|255|1|A1|C|||||123|||");
            //LIMIT ORDER - STANDARD
            //byte[] newOrderRequest = etiRequest.getByteArrForNewOrderSingleRequest("216|10100|||4|652101004|204950000000|||1111111111111088|||||999|50|50|||532540|||||30|1|1|2|0|0|2|255|1|A1|C|||||123|||");
            //byte[] newOrderRequest = etiRequest.getByteArrForNewOrderSingleRequest("216|10100|||4|708601002|184950000000|||1111111111111088|||||999|50|50|||532540|||||30|0|1|2|0|0|2|255|1|A1|C|||||123|||");

            //STOP LIMIT ORDER
            ////byte[] newOrderRequest = etiRequest.getByteArrForNewOrderSingleRequest("216|10100|||4|708601002|205900000000|204900000000||1111111111111088|||||999|50|50|||532540|||||30|0|1|4|0|0|2|255|1|A1|C|||||123|||");

            etiSession.sendMessage(newOrderRequest);


            /*
            //Parse the Single Order Response
            byte[] newOrderResponse = etiResponse.getByteArrForOrderSingleResponse(etiSession.receiveMessage());

            if (newOrderResponse != null) {
               System.out.println("ETI - New Order Response received: " + newOrderResponse.length);
            }
            */


            TimeUnit.SECONDS.sleep(30);

            System.out.println("---------------------------------------");
            String OrderID = String.valueOf(etiResponse.getOrderID()).trim();
            long ActivityTime = etiResponse.getActivityTime();

            //Make a Replace Order Request
            System.out.println("ETI Transaction - Replace Order");
               System.out.println("---------------------------------------");
            //REPLACE ORDER LEAN
            ////byte[] replaceOrderRequest = etiRequest.getByteArrForReplaceOrderSingleRequest("248|10106|||5|708601002|" + OrderID + "|||207005000000|||1111111111111088|" + ActivityTime + "||||999|100|100|||532540|708601002|||||30|0|1|2|0|0|2|255|1||A1|C|||||WIA99999|||");
            ////byte[] replaceOrderRequest = etiRequest.getByteArrForReplaceOrderSingleRequest("248|10106|||5|708601002|" + OrderID + "|||207005000000|||1111111111111088|" + ActivityTime + "||||999|100|100|||532540|708601002|||||30|0|1|2|0|0|2|255|1||A1|C|||||WIA99999|||");

            ////byte[] replaceOrderRequest = etiRequest.getByteArrForReplaceOrderSingleRequest("248|10106|||5|708601002|" + OrderID + "|||205600000000|||1111111111111088|" + ActivityTime + "||||999|100|100|||532540|708601002|||||30|1|1|2|0|0|2|255|1||A1|C|||||WIA99999|||");
            byte[] replaceOrderRequest = etiRequest.getByteArrForReplaceOrderSingleRequest("248|10106|||5|708601002|" + OrderID + "|||215005000000|||1111111111111088|" + ActivityTime + "||||999|100|100|||532540|708601002|||||30|1|1|2|0|0|2|255|1||A1|C|||||WIA99999|||");


            //REPLACE ORDER STANDARD
            //byte[] replaceOrderRequest = etiRequest.getByteArrForReplaceOrderSingleRequest("248|10106|||5|708601002|" + OrderID + "|||205005000000|||1111111111111088|" + ActivityTime + "||||999|100|100|||532540|708601002|||||30|1|1|2|0|0|2|255|1||A1|C|||||WIA99999|||");
            ////byte[] replaceOrderRequest = etiRequest.getByteArrForReplaceOrderSingleRequest("248|10106|||5|652101004|" + OrderID + "|||205005000000|||1111111111111088|" + ActivityTime + "||||999|100|100|||532540|652101004|||||30|1|1|2|0|0|2|255|1||A1|C|||||WIA99999|||");

            etiSession.sendMessage(replaceOrderRequest);

            /*
            //Parse the Single Order Response
            byte[] replaceOrderResponse = etiResponse.getByteArrForOrderSingleResponse(etiSession.receiveMessage());

            if (newOrderResponse != null) {
                System.out.println("ETI - New Order Response received: " + newOrderResponse.length);
            }
            */






            TimeUnit.SECONDS.sleep(30);
            System.out.println("Readying to cancel order ##################################");

            //Make a Cancel Order Request
            Instant instant = Instant.now();
            long epochTime = instant.getEpochSecond();

            System.out.println("ETI Transaction - Cancel Order Request");
            System.out.println("---------------------------------------");
            OrderID = String.valueOf(etiResponse.getOrderID()).trim();
            ActivityTime = etiResponse.getActivityTime();
            //System.out.println("Primary Order to CANCEL is:" + String.valueOf(etiResponse.getPrimaryOrderID()));
            //Cancel Order Lean
            //byte[] cancelOrderRequest = etiRequest.getByteArrForCancelOrderSingleRequest("96|10109|||6|708601002|" + OrderID + "|||" + ActivityTime + "|999||532540|708601002|||");
            //Cancel Order Standard
            //byte[] cancelOrderRequest = etiRequest.getByteArrForCancelOrderSingleRequest("96|10109|||6|652101004|" + OrderID + "|||" + ActivityTime + "|999||532540|652101004|||");
            byte[] cancelOrderRequest = etiRequest.getByteArrForCancelOrderSingleRequest("96|10109|||6|708601002|" + OrderID + "|||" + ActivityTime + "|999||532540|708601002|||");
            //byte[] cancelOrderRequest = etiRequest.getByteArrForCancelOrderSingleRequest("72|10109|||5|708601002|0|||||500112||708601002||");
            etiSession.sendMessage(cancelOrderRequest);
            TimeUnit.SECONDS.sleep(10);




            /*
            System.out.println("ETI Transaction - Cancel Order Request Again");
            System.out.println("Primary Order to CANCEL AGAIN is:" + OrderID);
            byte[] cancelOrderRequest2 = etiRequest.getByteArrForCancelOrderSingleRequest("72|10109|||6|708601002|" + OrderID  + "|111999||||500112||708601002||");
            etiSession.sendMessage(cancelOrderRequest2);
            */

            //TimeUnit.SECONDS.sleep(10);


            /*
            //Parse the Cancel Order Confirmation
            byte[] cancelOrderResponse = etiResponse.getByteArrForOrderSingleResponse(etiSession.receiveMessage());

            System.out.println("Cancel Order Response EXPECTED");

            //Parse the Cancel Order Response
            byte[] cancelOrderResponse2 = etiResponse.getByteArrForCancelOrderResponse(etiSession.receiveMessage());

            if (cancelOrderResponse2 != null) {
                System.out.println("ETI -  Cancel Order Response received: " + cancelOrderResponse2.length);
            }

            if (cancelOrderResponse != null) {
                System.out.println("ETI -  Cancel Order Confirmation received: " + cancelOrderResponse.length);
            }

            if (cancelOrderResponse2 != null) {
                System.out.println("ETI -  Cancel Order Response received: " + cancelOrderResponse2.length);
            }


            TimeUnit.SECONDS.sleep(600);

            */


           TimeUnit.SECONDS.sleep(30);


            //Make a User Logout Request
            System.out.println("ETI User Logout");
            System.out.println("---------------------------------------");
            //byte[] userLogoutRequest = etiRequest.getByteArrForUserLogoutRequest("32|10029|||7||652101004||");
            byte[] userLogoutRequest = etiRequest.getByteArrForUserLogoutRequest("32|10029|||7||708601002||");
            etiSession.sendMessage(userLogoutRequest);



            /*
            //Parse the User Logout Response
            byte[] userLogoutResponse = etiResponse.getByteArrForUserLogoutResponse(etiSession.receiveMessage());

            if (userLogoutResponse != null) {
                System.out.println("ETI - Session Logon Response received: " + userLogoutResponse.length);
            }

            */



            TimeUnit.SECONDS.sleep(5);

            System.out.println("ETI System to Close");

            //Make a Session Logout Request
            System.out.println("ETI Session Logout Request");
            System.out.println("---------------------------------------");
            byte[] sessionLogoutRequest = etiRequest.getByteArrForSessionLogoutRequest("24|10002|||8||");
            etiSession.sendMessage(sessionLogoutRequest);


            /*
            //Parse the Session Logout Response
            byte[] sessionLogoutResponse = etiResponse.getByteArrForSessionLogoutResponse(etiSession.receiveMessage());

            if (sessionLogoutResponse != null) {
                System.out.println("ETI - Session Logon Response received: " + sessionLogoutResponse.length);
            }
            */


            TimeUnit.SECONDS.sleep(5);
            
            System.out.println("ETI System Close");

            return;

    }catch (Exception e)
        {
             e.printStackTrace();
        }

    }
}